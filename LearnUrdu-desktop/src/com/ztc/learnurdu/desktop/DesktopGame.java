package com.ztc.learnurdu.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.ztc.learnurdu.LearnUrdu;

public class DesktopGame {

	public static void main(String[] args) {
		new LwjglApplication(new LearnUrdu(), "LearnUrdu", 480, 320);
	}
}
