package com.ztc.learnurdu.android;

import android.os.Bundle;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.ztc.learnurdu.LearnUrdu;

/**
 * 
 * @author Omer Gillani
 *
 */
public class AndroidGame extends AndroidApplication {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize(new LearnUrdu());
	}
}
